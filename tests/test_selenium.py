"""Módulo de testes da aplicação usando Selenium."""
from selenium.webdriver import Remote


def test_msg_bemvindo_has_the_right_content():
    """Preenche form e checa se conteúdo da msg bemvindo está como esperado."""
    person = "Maria"
    driver = Remote(
        command_executor='http://firefox:4444/wd/hub',
        desired_capabilities={'browserName': 'firefox'}
    )
    driver.implicitly_wait(10)
    driver.get('http://localhost')
    driver.find_element_by_id('name').send_keys(person)
    driver.find_element_by_css_selector('[value="Enviar"]').click()
    body_text = driver.find_element_by_tag_name('body').text
    driver.save_screenshot('selenium.png')
    driver.quit()
    print('[[ATTACHMENT|/selenium.png]]')
    assert body_text == f'Bem vindo(a) {person} !'
