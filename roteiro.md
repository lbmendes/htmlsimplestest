
Explicar Conceitos:
  - GIT: Ferramenta de controle de versões
  - Gitlab: Sistema gerenciador de repósitórios, baseado em git. Possui diversas funcionalidades relacionadas às atividades de software:
      - Gerenciamento de projetos
      - Registro de issues
      - Container Registry
      - Revisão de código
      - Monitoramento de aplicações
      - Testes
      - Verificações de segurança
      - Deploy
  - CI: ...
  - CD: ...
  - CDeployment: ...
  - Pipeline:
      - .gitlab-ci.yml
      - Gitlab Runner
          - formas de rodar gitlab runner
          - tipos de executor de gitlab runner



Passos a fazer

1. Criar novo repo "meurepo" no gitlab.com, inicializar projeto em branco com readme.

2. Clonar o repo na sua máquina local, preferencialmente com link de SSH:
  - git clone <url_para_clone>
  - cd meurepo
  - git status

3. Criar a aplicação. São dois arquivos index.html e bemvindo.php para a aplicação,
 um arquivo Dockerfile para a imagem que roda nosso app e outro para o .gitignore.

index.html

~~~html
<!DOCTYPE html>
<html>
    <head>
        <title>Titulo</title>
    </head>
    <body>
        <h2> O sistema possui as seguintes funcionalidades: </h2>
        <ul>
            <li> login ok </li>
            <li> cadastro top </li>
            <li> consulta ok </li>
        </ul>
        <form action="/bemvindo.php">
            <label for="name"> Nome:</label>
            <input type="text" id="name" name="name"><br>
            <input type="submit" value="Enviar">
        </form>
    </body>
</html>
~~~

bemvindo.php

~~~php
<!DOCTYPE html>
<html>
    <body>
        Bem vindo
        <?php
            echo $_REQUEST['name'];
        ?> !
    </body>
</html>
~~~

Dockerfile
~~~docker
FROM php:apache
COPY ./index.html ./bemvindo.php /var/www/html/
~~~

.gitignore
~~~
# Pasta do ambiente vitual python
venv/

# Pastas de arquivos de cache do python
__pycache__/

# arquivo de log do geckodriver-selenium
geckodriver.log

# diretório de reports
reports/

# print do selenium
selenium.png
~~~

4. Fazer teste na nossa máquina local. 
### Fazer build da imagem e subir o container

~~~bash
docker build -t imglocal .
docker container run -d -p 80:80 --name container_teste imglocal
~~~
Depois testar acesso no [navegador acessando localhost](http://localhost)

Neste ponto a aplicação aparentemente está funcionando bem. 

Ao finalizar o teste remova o container e a imagem:
~~~bash
docker container rm -f container_teste
docker image rm imglocal
~~~


### Atualizando o repo
  - Adicione os arquivos à área de staging (git add --all)
  - Verifique quais arquivos serão comitados (git status)
  - Realize o commit no repo local (git commit -m "Escreva aqui a msg do commit")
  - Envie as alterações do repo local para o gitlab (git push)


**Vamos começar o nosso .gitlab-ci.yml e ver como ele pode ajudar na qualidade e segurança do app.**

5. Iniciar o arquivo .gitlab-ci.yml com declaração dos stages e job de análise do html:

.gitlab-ci.yml

~~~yaml
stages:
  - static analysis
  - build
  - test
  - deploy

HTML static analysis:
  stage: static analysis
  script:
    - 'staticcheck=$(curl -H "Content-Type: text/html; charset=utf-8" --data-binary @index.html https://validator.w3.org/nu/?out=json)'
    - echo $staticcheck
    - test "$staticcheck" == '{"messages":[]}'
~~~

`stages`: lista de stages que farão parte do pipeline

`stage`: declaração do stage a que o job é vinculado

`script`: lista de comandos que serão executados no job

Nesse ponto estamos criando um pipeline com 4 stages e definindo 1 job que faz parte do stage "static analysis".

Esse job envia o conteúdo da nossa página index.html para a api do w3 que checa se o HTML é válido. Ele imprime o resultado da checagem e testa se o resultado é um json com uma lista vazia (sinal de que não pendências).

[Atualize o repo](#atualizando-o-repo) e acompanhe o resultado da pipeline EM CI/CD --> Pipelines.

Verifique o resultado do job, analise problemas identificados, corrija código e atualize o repo novamente.

<details>
  <summary> Ver Correção </summary>
  Substituir tag &lt;html&gt; por &lt;html lang="pt-BR"&gt;


</details>

__________


6. Adicionar job de build docker image:

Aqui começaremos o uso do container registry do próprio gitlab. Primeiramente acrescente uma variavel de escopo global DOCKER_IMAGE no início do .gitlab-ci.yml

~~~yaml
variables:
  DOCKER_IMAGE: "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/apphtml"
~~~

Depois acrescente ao final do mesmo arquivo o job build docker image.

~~~yaml
build docker image:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/apphtml . 
    - docker push $DOCKER_IMAGE
~~~

`image`: indicação da imagem que será usada no job

`services`: lista com indicação de imagens que serão executadas em background como serviço para apoiar execução

`before_script`: lista de comandos que serão executados antes do script principal do job


Nesse job foi verificado alguns problemas eventuais de lentidão/timeout usando os runners do gitlab.com.

Para evitar isso vamos criar um gitlab-runner local em um container docker.

### Rodando um container docker como gitlab-runner local
~~~bash
# cria um volume docker específico para o runner
docker volume create gitlab-runner-config

# roda o container do gitlab-runner
# OBS: a imagem padrão usa ubuntu e é muito grande (2,19GB na v14.3)
docker run -d --name gitlab-runner --restart always \
    --env TZ="America/Bahia" \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:latest
~~~

### Registrando o container gitlab-runner como executor docker
~~~bash
# comando para registrar config do runner no volume docker "gitlab-runner-config"
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "INSERT_REGISTRATION_TOKEN" \
  --description "Runner Local Docker" \
  --tag-list "runner-local-docker" \
  --executor "docker" \
  --docker-image alpine:latest \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

# OBS: O Token de registro consta na pagina do repo, Settings --> CI/CD e expandir seção "Runners"

~~~

Feitos os procedimentos acima, consultar pagina do repo, Settings --> CI/CD, 
seção "Runners", e checar se consta o runner com a tag runner-local-docker.

Se o runner constar mas após uns 3 minutos não estiver com o status ok (verde), reinicie o container do runner.
~~~bash
docker restart gitlab-runner
~~~

Ajuste o .gitlab-ci.yml para fazer constar a tag do runner no job de build:

~~~yaml
build docker image:
  stage: build
  tags:
    - runner-local-docker 
  ...
~~~

`tag`: indicação de tag. Se houver runner com a mesma tag o job será direcionado para execução neste runner


[Atualize o repo](#atualizando-o-repo) e acompanhe o resultado da pipeline EM CI/CD --> Pipelines.


7. Iniciar stage de testes adicionando jobs de análise SAST (análise estática de segurança) e Container Scanning.

Adicione após o job de build o seguinte trecho no .gitlab-ci.yml
~~~yaml
include:
  - template: Security/SAST.gitlab-ci.yml
~~~

`include`: inclusão de arquivo externo como parte do pipeline.


Ele inclui o job default do gitlab que faz a análise do código do repositório buscando identificar vulnerabilidades.

[Atualize o repo](#atualizando-o-repo), verifique a pipeline, analise problemas identificados e corrija código.

<details>
  <summary> Ver Correção </summary>
  Substituir no arquivo bemvindo.php echo $_REQUEST['name'] por echo htmlspecialchars($_REQUEST['name'])


</details>

__________


Além da análise SAST vamos incluir o job default Container Scanning do Gitlab.

~~~yaml
include:
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml
~~~

**Importante: No container scanning será avaliada a imagem que estiver indicada na variável $DOCKER_IMAGE**

[Atualize o repo](#atualizando-o-repo), verifique a pipeline, analise problemas identificados e corrija código.

<details>
  <summary> Ver Correção </summary>


  Dada a quantidade tão grande de vulnerabilidades identificadas a nossa abordagem vai ser por
  alterar imagem do base do Dockerfile, passaremos a usar alpine em substituição à php:apache.

  Dockerfile
  ~~~docker
  FROM alpine
  RUN apk --no-cache add apache2 php8-apache2 tzdata
  COPY ./index.html ./bemvindo.php /var/www/localhost/htdocs/
  EXPOSE 80
  ENV TZ="America/Bahia"
  ENTRYPOINT ["httpd", "-D", "FOREGROUND"]
  ~~~

  Antes de atualizar o repo, faça o teste local da nova imagem seguindos [as orientações de build local](#fazer-build-da-imagem-e-subir-o-container)


</details>

__________


8. Acrescentar testes da aplicação usando python

Na pasta raiz do projeto crie uma pasta tests e acrescente o seguinte arquivo:

tests/test_app.py
~~~python
"""Módulo de testes da aplicação usando requisições http."""
from urllib import request
from pytest import mark


def test_response_code_is_200():
    """Envia requisição http para a aplicação e checa se cód. retorno é 200."""
    with request.urlopen('http://localhost/') as response:
        response_code = response.code
    assert response_code == 200


@mark.parametrize(
    'feature',
    ['login', 'cadastro', 'consulta']
)
def test_features_are_working(feature):
    """Verifica se funcionalidade indicada está OK."""
    with request.urlopen('http://localhost/') as response:
        html = response.read()
    assert f'{feature} ok' in str(html)

~~~

Esse código valida se a aplicação está retornando código 200 e se as funcionalidades estão ok.

Para rodar o teste localmente na sua máquina, primeiramente vamos
##### Preparar ambiente de testes
~~~bash
# IMPORTANTE COMANDOS DEVEM SER EXECUTADOS A PARTIR DA PASTA RAIZ DO PROJETO
python3 -m venv venv  # para criar um ambiente virual (Virtual ENVironment) python específico do projeto.
source venv/bin/activate  # para ativar o ambiente virtual recém criado
pip install pytest selenium webdriver_manager  # instalando as libs necessárias pros testes no ambiente virtual python
~~~

Agora rode um container da aplicação ([instruções aqui](#fazer-build-da-imagem-e-subir-o-container)), e execute o teste:

~~~bash
pytest -v tests/test_app.py
~~~

A qualquer momento o ambiente virtual python pode ser desativado com o comando **deactivate**

Com o teste local já vai ficar claro onde está o problema mas antes de corrigir vamos acrescentar o job desse teste o .gitlab-ci.

.gitlab-ci.yml
~~~yml
test app:
  stage: test
  image:
    name: $DOCKER_IMAGE
    entrypoint: ["/bin/sh", "-c"]
  before_script:
    - apk --no-cache add python3
    - python3 -m ensurepip --default-pip
    - pip install pytest
    - httpd
    - sleep 5
  script:
    - pytest -v --junitxml reports/test_app.xml tests/test_app.py
  artifacts:
    when: always
    paths:
      - ./reports/test_app.xml
    reports:
      junit: ./reports/test_app.xml
~~~

`artifacts`: indicação de arquivos/relatórios que serão salvos no pipeline e estarão disponíveis para download

Agora vamos atualizar o repo no gitlab e verificar como a falha vai constar no pipeline.

Efetue a correção e atualize o repo novamente.

<details>
  <summary> Ver Correção </summary>
  Substituir  no index.html "cadastro top" por "cadastro ok, top"


</details>

__________


9. Acrescentar testes fim-a-fim usando selenium com python

Para implementar os testes com selenium vamos acrescentar dois arquivos test_selenium.py e test_selenium_local.py.

Eles fazem a mesma validação mas o teste local irá apresentar em tela o resultado viusual em tempo de execução.

Esse teste realiza as ações de:
  - abrir navegador, 
  - acessar aplicação, 
  - preencher/submeter formulário
  - verificar se conteúdo mostrado é o conteúdo esperado

test_selenium.py
~~~python
"""Módulo de testes da aplicação usando Selenium."""
from selenium.webdriver import Remote


def test_msg_bemvindo_has_the_right_content():
    """Preenche form e checa se conteúdo da msg bemvindo está como esperado."""
    person = "Maria"
    driver = Remote(
        command_executor='http://firefox:4444/wd/hub',
        desired_capabilities={'browserName': 'firefox'}
    )
    driver.implicitly_wait(10)
    driver.get('http://localhost')
    driver.find_element_by_id('name').send_keys(person)
    driver.find_element_by_css_selector('[value="Enviar"]').click()
    body_text = driver.find_element_by_tag_name('body').text
    driver.save_screenshot('selenium.png')
    driver.quit()
    print('[[ATTACHMENT|/selenium.png]]')
    assert body_text == f'Bem vindo(a) {person} !'

~~~

test_selenium_local.py
~~~python
"""Módulo de testes da aplicação usando Selenium."""
from selenium.webdriver import Firefox
from time import sleep
from webdriver_manager.firefox import GeckoDriverManager


def test_msg_bemvindo_has_the_right_content():
    """Preenche form e checa se conteúdo da msg bemvindo está como esperado."""
    person = "Maria"
    driver = Firefox(executable_path=GeckoDriverManager().install())
    driver.get('http://localhost/')
    sleep(3)
    driver.find_element_by_id('name').send_keys(person)
    sleep(3)
    driver.find_element_by_css_selector('[value="Enviar"]').click()
    sleep(3)
    body_text = driver.find_element_by_tag_name('body').text
    driver.save_screenshot('selenium.png')
    driver.quit()
    assert body_text == f'Bem vindo(a) {person} !'

~~~

Para rodar os testes localmente, considerando que o [ambiente de testes está preparado](#preparar-ambiente-de-testes) e o ambiente virtual está ativado, execute o seguinte comando:
~~~bash
pytest -v tests/test_selenium_local.py
~~~

Não vamos corrigir o erro agora, primeiro vamos incluir um job para o teste com selenium no pipeline.

.gitlab-ci.yml
~~~yml
test selenium:
  stage: test
  services:
    - name: selenium/standalone-firefox
      alias: firefox
  image:
    name: $DOCKER_IMAGE
    entrypoint: ["/bin/sh", "-c"]
  before_script:
    - apk --no-cache add python3
    - python3 -m ensurepip --default-pip
    - pip install selenium pytest
    - meuip=$(ip a | grep 172 | awk '{ print $2 }' | cut -f1 -d/)
    - sed -i "s/localhost/$meuip/g" tests/test_selenium.py
    - httpd
    - sleep 5
  script:
    - pytest -v --junitxml reports/test_selenium.xml -o junit_logging=system-out tests/test_selenium.py
  artifacts:
    when: always
    paths:
      - ./reports/test_selenium.xml
      - ./selenium.png
    reports:
      junit: ./reports/test_selenium.xml
~~~

Alternativa para validar, usando app principal como serviço e imagem do job python:
~~~yaml
test selenium:
  stage: test
  services:
    - name: selenium/standalone-firefox
      alias: firefox
    - name: $DOCKER_IMAGE
      alias: meuapp
  image: python:slim
  before_script:
    - pip install selenium pytest
    - cat /etc/hosts
    - meuip=$(grep meuapp /etc/hosts | awk '{ print $1 }')
    - sed -i "s/localhost/$meuip/g" tests/test_selenium.py
  script:
    - pytest -v --junitxml reports/test_selenium.xml -o junit_logging=system-out tests/test_selenium.py
  artifacts:
    when: always
    paths:
      - ./reports/test_selenium.xml
      - ./selenium.png
    reports:
      junit: ./reports/test_selenium.xml
~~~

Atualize o repositório do gitlab, avalie o resultado, veja os detalhes dos relatórios de testes e depois corrija o código.

<details>
  <summary> Ver Correção </summary>
  Substituir  no bemvindo.php "Bem vindo" por "Bem vindo(a)"


</details>

__________

10. Configurar deploy para a sua máquina local

O deploy partirá do pipeline no gitlab e deve subir a aplicação na nossa máquina local.

Para tanto iremos:
  - registrar gitlab runner local com executor shell
  - configurar troca de chaves do container runner --> nossa máquina local
  - criar job de deploy


### Registrando o container gitlab-runner como executor shell
~~~bash
# comando para registrar config do runner no volume docker "gitlab-runner-config"
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "INSERT_REGISTRATION_TOKEN" \
  --description "Runner Local Shell" \
  --tag-list "runner-local-shell" \
  --executor "shell"

# OBS: O Token de registro consta na pagina do repo, Settings --> CI/CD e expandir seção "Runners"

~~~

Feitos os procedimentos acima, consultar pagina do repo, Settings --> CI/CD, 
seção "Runners", e checar se consta o runner com a tag runner-local-shell.

Se o runner constar mas após uns 3 minutos não estiver com o status ok (verde), reinicie o container do runner.
~~~bash
docker restart gitlab-runner
~~~

### Configurando troca de chaves no gitlab runner
~~~bash
docker container exec -ti gitlab-runner bash  # iniciando um bash dentro do container gitlab-runner
su gitlab-runner  # altere o usuário para o user gitlab-runner
cd   # indo para home do usuário
ssh-keygen -a 100 -t ed25519  # gera par de chaves do usuário gitlab-runner
cat ~/.ssh/id_ed25519.pub | ssh usuario_que_faz_deploy@ip_da_maquina_do_deploy "cat >>  ~/.ssh/authorized_keys"
exit  # sai da sessão do user gitlab-runner no container
exit  # encerra sessão bash no container
~~~

Com as configurações acima o usuário gitlab-runner poderá acessar o usuário de deploy na máquina de deploy sem necessidade de criação de variáveis para credenciais ou chaves de criptografia.

Agora vamos criar o job de deploy onde será utilizado o executor shell do gitlab runner para, por meio de conexão ssh, realizar os comandos para pôr no ar a aplicação.

.gitlab-ci.yml
~~~yml
deploy app:
  stage: deploy
  tags:
    - runner-local-shell
  script:
  - ssh $DEPLOY_USER@$DEPLOY_IP "if (docker ps | grep meu_app) then docker rm -f meu_app; fi; docker container run -d -p 80:80 --name meu_app $DOCKER_IMAGE"

~~~

Configuramos no script o uso de duas variáveis:
  - DEPLOY_USER: Usuário que vai fazer o deploy
  - DEPLOY_IP: IP da máquina em que será feito o deploy.

Crie estas variáveis em: Página do repo --> Settings --> CI/CD, expandir seção Variables --> Add Variable

Agora atualize o seu repositório no gitlab e veja a pipeline rodar do inicio ao fim, todos os stages definidos.

:)

![](/palmas.gif)
